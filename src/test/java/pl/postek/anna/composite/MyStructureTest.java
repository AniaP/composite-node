package pl.postek.anna.composite;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.postek.anna.composite.file.Directory;
import pl.postek.anna.composite.file.File;

import static org.junit.jupiter.api.Assertions.*;

class MyStructureTest {

    private static final MyStructure structure = new MyStructure();
    private static final File pic = new File("pic.jpg", "/pic");
    private static final File cv = new File("cv.pdf", "/cv");
    private static final Directory dir = new Directory("home", "/home");
    private static final Directory host = new Directory("host", "/host");

    @BeforeAll
    static void control() {
        dir.addNode(pic);
        dir.addNode(cv);
        dir.addNode(host);
        host.addNode(pic);
        structure.addNode(pic);
        structure.addNode(cv);
        structure.addNode(dir);
        structure.addNode(host);
    }

    @Test
    void test_true_find_by_code_from_INode() {
        assertTrue(structure.findByCode("pic.jpg").equals(pic));
    }

    @Test
    void test_true_find_by_code_from_ICompositeNode() {
        assertEquals(true, structure.findByCode("home").equals(dir));
    }

    @Test
    void test_null_find_by_renderer_from_INode() {
        assertNull(structure.findByRenderer("xxx"));
    }

    @Test
    void test_count_number_of_nodes() {
        assertEquals(6, structure.count());
    }

}