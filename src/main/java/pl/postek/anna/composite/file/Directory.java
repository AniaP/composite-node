package pl.postek.anna.composite.file;

import pl.postek.anna.composite.ICompositeNode;
import pl.postek.anna.composite.INode;

import java.util.ArrayList;
import java.util.List;

public class Directory implements ICompositeNode {
    private String code;
    private String renderer;
    private List<INode> nodes = new ArrayList<>();

    public Directory(String code, String renderer) {
        this.code = code;
        this.renderer = renderer;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getRenderer() {
        return renderer;
    }

    @Override
    public List<INode> getNodes() {
        return nodes;
    }

    @Override
    public int count() {
        return nodes.stream().mapToInt(INode::count).sum();
    }
    public void addNode(INode node) {
        nodes.add(node);
    }

    @Override
    public String toString() {
        return "Directory{" +
                "code='" + code + '\'' +
                ", renderer='" + renderer + '\'' +
                ", nodes=" + nodes +
                '}';
    }
}
