package pl.postek.anna.composite.file;

import pl.postek.anna.composite.INode;
import pl.postek.anna.composite.MyStructure;

public class Main {

    public static void main(String[] args) {
        Directory rootDirectory = new Directory("root", "/root");
        Directory homeDirectory = new Directory("home", "/home");
        File documentFile = new File("cv.doc", "/home/cv");
        File pictureFile = new File("pic.jpg", "/home/pic");
        File picture2 = new File("enjoy.jpg", "/home/enjoy");

        homeDirectory.addNode(documentFile);
        homeDirectory.addNode(pictureFile);
        rootDirectory.addNode(homeDirectory);
        rootDirectory.addNode(picture2);

        MyStructure structure = new MyStructure();
        structure.addNode(documentFile);
        structure.addNode(pictureFile);
        structure.addNode(picture2);
        structure.addNode(rootDirectory);

        System.out.println("Structure count:"  + structure.count());
        System.out.println("All structure contains: " + structure.toString());
        for (INode node : rootDirectory.getNodes()) {
            System.out.println(node + " has codes: " + node.getCode());
            System.out.println(node + " has renderer: " + node.getRenderer());
        }
        System.out.println("RootDirectory has: " + rootDirectory.count());
        System.out.println("HomeDirectory has: " + homeDirectory.count());

        System.out.println("Show file by code");
        System.out.println(structure.findByCode("cv.doc"));
        System.out.println(structure.findByRenderer("/home/cv"));

        System.out.println(structure.findByCode("home"));
        structure.addNode(homeDirectory);

        System.out.println(structure.findByCode("home"));
        System.out.println(structure);
    }
}
