package pl.postek.anna.composite.file;

import pl.postek.anna.composite.INode;

public class File implements INode {
    private String code;
    private String renderer;

    public File(String code, String renderer) {
        this.code = code;
        this.renderer = renderer;
    }
    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getRenderer() {
        return renderer;
    }

    @Override
    public int count() {
        return 1;
    }

    @Override
    public String toString() {
        return "File{" +
                "code='" + code + '\'' +
                ", renderer='" + renderer + '\'' +
                '}';
    }
}
