package pl.postek.anna.composite;

public interface INode {
    String getCode();
    String getRenderer();
    int count();
}
