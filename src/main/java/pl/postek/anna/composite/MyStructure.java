package pl.postek.anna.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class MyStructure implements IMyStructure {
    private List<INode> nodes = new ArrayList<>();

    @Override
    public INode findByCode(String code) {
        return findByPredicate(iNode -> iNode.getCode().equals(code));
    }

    @Override
    public INode findByRenderer(String renderer) {
        return findByPredicate(iNode -> iNode.getRenderer().equals(renderer));
    }

    private INode findByPredicate(Predicate<INode> predicate) {
        Optional<INode> first = nodes.stream().filter(predicate).findFirst();
        return first.orElse(null);
    }

    @Override
    public int count() {
        return nodes.stream().mapToInt(INode::count).sum();
    }

    public void addNode(INode node) {
        nodes.add(node);
    }

    @Override
    public String toString() {
        return "MyStructure{" +
                "nodes=" + nodes +
                '}';
    }
}
